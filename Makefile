CC       = g++
CPPFLAGS = -Wall -Wextra -O2 -std=c++17 
RFLAGS = -lstdc++fs

serwer: serwer.o 
	g++ $(CPPFLAGS) -o serwer serwer.o $(RFLAGS)

serwer.o: serwer.cpp
	g++ $(CPPFLAGS) -c serwer.cpp $(RFLAGS)
clean:
	rm -f *.o serwer
