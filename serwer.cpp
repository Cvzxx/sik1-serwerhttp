#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <cstdio>
#include <unistd.h>
#include <string>
#include <iostream>
#include <filesystem>
#include <regex>
#include <vector>
#include <ext/stdio_filebuf.h>
#include <unordered_map>

#define QUEUE_LENGTH     5
#define PORT_NUM     8080

int sock, msg_sock;
std::regex request_pattern(R"(^([A-Z]+)[ ](\/[a-zA-Z0-9.\-\/]*)[ ](HTTP\/1\.1)$)");
std::regex valid_request_pattern(R"(^(GET|HEAD)[ ](\/[a-zA-Z0-9.\-\/]*)[ ](HTTP\/1\.1)$)");
std::regex header_pattern(R"(^(([a-zA-Z])+:[ ]*[a-zA-Z0-9.\-\/]+)$)", std::regex_constants::icase);
std::regex valid_header_pattern(R"(^((Connection):[ ]*(close))|((Content-Length):[ ]*(0))$)",
                                std::regex_constants::icase);
std::string LINE_END = "\r\n";
std::unordered_map<std::string, std::string> headers_map;
std::string method_type;
std::string dir_path;
std::string server_path;
std::string file_path;
std::string mess_body;
std::string buffer;

void clear() {
    headers_map.clear();
    file_path = "";
    mess_body = "";
    buffer = "";
}

void close_socket() {
    if (close(msg_sock) < 0) {
        perror("close");
    }
}

void file_not_found() {
    u_long len;
    std::string send =
            "HTTP/1.1 404 not file found\r\n";

    if (headers_map.find("CONNECTION") != headers_map.end()) {
        std::string closed = "Connection:close\r\n";
        send += closed;
    }
    send += "\r\n";

    len = write(msg_sock, send.c_str(), send.length());
    if (len != send.length()) {
        perror("writing to client socket");
        clear();
        close_socket();
    }

    if (headers_map.find("CONNECTION") != headers_map.end()) {
        close_socket();
    }
}

void handle_correlated_server(bool &found) {
    std::ifstream file = std::ifstream(server_path);
    std::string line;
    size_t len;
    if (file.is_open()) {
        while (std::getline(file, line)) {
            std::istringstream is(line);
            std::string target_path, ip_server, port_num;

            is >> target_path >> ip_server >> port_num;
            if (target_path == file_path) {
                found = true;
                std::string send =
                        "HTTP/1.1 302 found file\r\nLocation:http://" + ip_server + ":" + port_num +
                        target_path + "\r\n\r\n";
                len = write(msg_sock, send.c_str(), send.length());
                if (len != send.length()) {
                    perror("writing to client socket");
                    clear();
                    close_socket();
                }

                break;
            }

        }
    } else {
        std::string send =
                "HTTP/1.1 500 ERROR FILE\r\nConnection:close\r\n\r\n";
        len = write(msg_sock, send.c_str(), send.length());
        if (len != send.length()) {
            perror("writing to client socket");
            clear();
            close_socket();
        }
        perror("nie istnieje albo problem z otwaricem pliku");
        exit(EXIT_FAILURE);
    }
}

void handle_file(bool &found, std::ifstream &file) {
    found = true;
    std::string line;
    u_long len;
    std::uintmax_t size = std::filesystem::file_size(dir_path + file_path);
    while (std::getline(file, line)) {
        mess_body += line + "\n";
    }

    mess_body = mess_body.substr(0, mess_body.length() - 1);

    std::string send =
            "HTTP/1.1 200 found file\r\nContent-Length:" + std::to_string(size) +
            "\r\nContent-Type:application/octet-stream\r\n";
    if (headers_map.find("CONNECTION") != headers_map.end()) {
        std::string closed = "Connection:close\r\n";
        send += closed;
    }
    send += LINE_END;

    buffer += send;
    if (method_type == "GET")
        buffer += mess_body;

    len = write(msg_sock, buffer.c_str(), buffer.length());
    if (len != buffer.length()) {
        perror("writing to client socket");
        clear();
        close_socket();
    }

    if (headers_map.find("CONNECTION") != headers_map.end()) {
        close_socket();
    }
}

void task_handler() {

    if (method_type == "GET" || method_type == "HEAD") {
        std::ifstream file(dir_path + file_path);
        std::string line;
        bool found = false;
        bool is_dir;
        u_long len;
        try {
            is_dir = std::filesystem::is_directory(dir_path + file_path);
        } catch (std::exception &e) {
            std::string send =
                    "HTTP/1.1 404 not file found\r\n\r\n";

            len = write(msg_sock, send.c_str(), send.length());
            if (len != send.length()) {
                perror("writing to client socket");
                clear();
                close_socket();
            }
            return;
        }

        if (!is_dir && file.is_open()) {
            handle_file(found, file);
        } else {
            handle_correlated_server(found);
        }

        if (!found) {
            file_not_found();
        }
    }
}

bool helper_vpf(size_t start, size_t end, std::string &copy, int32_t &counter) {
    std::string temp = copy.substr(start, end - start);
    if (temp == "..")
        counter--;
    else
        counter++;

    if (counter < 0) {
        return false;
    }

    return true;
}

bool validate_path_file() {
    size_t start = 0;
    std::string del = "/";
    std::string copy = file_path.substr(1, file_path.length());
    size_t end = copy.find(del);
    int32_t counter = 0;

    while (end != std::string::npos) {
        if (!helper_vpf(start, end, copy, counter)) {
            return false;
        }
        start = end + del.size();
        end = copy.find(del, start);
    }
    return helper_vpf(start, end, copy, counter);
}

int start_line_handler(std::string &temp) {
    std::smatch match;
    if (!std::regex_search(temp, match, request_pattern)) {
        perror("problem with start-line/incorrect input");
        return -1;
    }

    if (!std::regex_search(temp, match, valid_request_pattern)) {
        perror("problem with method name");
        return -2;
    }

    method_type = match[1].str();
    file_path = match[2].str();

    if (!validate_path_file()) {
        return -3;
    }
    return 1;
}


int request_handler(std::string temp) {

    std::smatch matches;
    if (std::regex_match(temp, matches, header_pattern)) {
        if (std::regex_match(temp, matches, valid_header_pattern)) {
            if (matches[3].str() == "close") {
                if (headers_map.find("CONNECTION") != headers_map.end())
                    return -1;
                else {
                    headers_map.insert({"CONNECTION", "CLOSE"});
                }
            } else {
                if (headers_map.find("CONTENT-LENGTH") != headers_map.end())
                    return -1;
                else
                    headers_map.insert({"CONTENT-LENGTH", matches[6].str()});

                return 1;
            }
        }

        return 1;
    } else {
        return -2;
    }
}


int start_line_errors(std::string &msg) {
    int val = start_line_handler(msg);
    u_long len;
    if (val == -1) {
        std::string wrong_start_line = "HTTP/1.1 400 wrong start-line input\r\nConnection:close\r\n\r\n";
        len = write(msg_sock, wrong_start_line.c_str(), wrong_start_line.length());
        if (len != wrong_start_line.length()) {
            perror("writing to client socket");
        }
        close_socket();
        return -1;
    } else if (val == -2) {
        std::string wrong_method_name = "HTTP/1.1 501 wrong method name\r\nConnection:close\r\n\r\n";
        len = write(msg_sock, wrong_method_name.c_str(), wrong_method_name.length());
        if (len != wrong_method_name.length()) {
            perror("writing to client socket");
        }
        close_socket();
        return -1;
    } else if (val == -3) {
        std::string send =
                "HTTP/1.1 404 out of current directory\r\n";
        if (headers_map.find("CONNECTION") != headers_map.end()) {
            std::string closed = "Connection:close\r\n";
            send += closed;
        }
        send += "\r\n";

        len = write(msg_sock, send.c_str(), send.length());
        if (len != send.length()) {
            perror("writing to client socket");
            clear();
            close_socket();
        }

        if (headers_map.find("CONNECTION") != headers_map.end()) {
            close_socket();
            return -3;
        }
        clear();
        return -2;
    }
    return 1;
}

int reguest_error(std::string &msg) {
    int val = request_handler(msg);
    u_long len;
    if (val == -1) {

        std::string dup_headers = "HTTP/1.1 400 DUPLICATE HEADERS input\r\nConnection:close\r\n\r\n";
        len = write(msg_sock, dup_headers.c_str(), dup_headers.length());
        if (len != dup_headers.length()) {
            perror("writing to client socket");
        }
        close_socket();
        return -1;
    } else if (val == -2) {
        std::string wrong_header = "HTTP/1.1 400 WRONG HEADERS input\r\nConnection:close\r\n\r\n";
        len = write(msg_sock, wrong_header.c_str(), wrong_header.length());
        if (len != wrong_header.length()) {
            perror("writing to client socket");
        }
        close_socket();
        return -1;
    }

    return 1;
}

void request_exec() {
    __gnu_cxx::stdio_filebuf<char> sourcebuf(msg_sock, std::ios::out | std::ios::in);
    std::iostream out(&sourcebuf);
    std::string msg;

    size_t pos = 0;
    size_t line_counter = 0;
    bool flag = false;
    while (std::getline(out, msg)) {
        if (msg.size() == 1 && msg == "\r") {
            flag = true;
        } else if
                ((pos = msg.find(LINE_END)) != std::string::npos || (pos = msg.find('\n')) != std::string::npos ||
                 (pos = msg.find('\r')) != std::string::npos) {
            msg = msg.substr(0, pos);
        }
        if (line_counter == 0) {
            int val = start_line_errors(msg);

            if (val == -1 || val == -3) {
                break;
            } else if (val == -2) {
                line_counter = -1;
                flag = false;
            }

        } else {
            if (flag) {
                task_handler();
                line_counter = -1;
                flag = false;
                clear();
            } else {
                if (reguest_error(msg) == -1) {
                    break;
                }
            }

        }
        line_counter++;
    }
}

bool path_handler(std::string arg) {
    try {
        if (!std::filesystem::exists(arg))
            return false;

    } catch (std::exception &e) {
        perror("ZLY ARG");
        exit(EXIT_FAILURE);
    }

    return true;
}


int main(int argc, char *argv[]) {
    // handling too  much args?
    if (argc < 3) {
        exit(1); // error za malo argsow - do poprawnie
    }

    in_port_t port;
    if (argc == 4) {
        int temp(std::stoi(argv[3]));

        if (temp <= static_cast<int>(UINT16_MAX) && temp >= 0) {
            port = static_cast<uint16_t>(temp);
        } else {
            perror("problem with casthing input portnum");
            exit(1);
        }
    }

    dir_path = argv[1];
    server_path = argv[2];
    if (!path_handler(dir_path)) {
        perror("NIE ISTNIEJE");
        exit(EXIT_FAILURE);
    }

    if (!path_handler(argv[2])) {
        perror("NIE ISTNIEJE");
        exit(EXIT_FAILURE);
    }

    struct sockaddr_in server_address;
    struct sockaddr_in client_address;
    socklen_t client_address_len;


    sock = socket(PF_INET, SOCK_STREAM, 0);
    if (sock < 0) {
        perror("socket");
        exit(1);
    }

    int enable = 1;
    if (setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int)) < 0) {
        perror("setsockopt(SO_REUSEADDR) failed");
        exit(1);
    }


    server_address.sin_family = AF_INET;
    server_address.sin_addr.s_addr = htonl(INADDR_ANY);
    if (argc < 4)
        server_address.sin_port = htons(PORT_NUM);
    else
        server_address.sin_port = htons(port);
    if (bind(sock, (struct sockaddr *) &server_address, sizeof(server_address)) < 0) {
        perror("bind");
        exit(1);
    }

    if (listen(sock, QUEUE_LENGTH) < 0) {
        perror("listen");
        exit(1);
    }

    printf("accepting client connections on port %hu\n", ntohs(server_address.sin_port));
    for (;;) {
        client_address_len = sizeof(client_address);
        msg_sock = accept(sock, (struct sockaddr *) &client_address, &client_address_len);
        clear();
        if (msg_sock < 0) {
            perror("accept");
            exit(1);
        }

        request_exec();
    }

    return 0;
}
